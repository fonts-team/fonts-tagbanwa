                               Tagbanwa font
			       =============

This is a font for the Tagbanwa script, distributed under the terms of
the Creative Commons Attribution 3.0 Unported license (see CC-3.0 file or
http://creativecommons.org/licenses/by/3.0/).

It was re-drawn from scratch by just looking at the unicode 5.0 tagbanwa chart
glyphs.  I also used the snippet at

http://www.omniglot.com/writing/tagbanwa.htm

for (crudely) fixing the marks. The following should more or less look the same
(utf-8 text):

ᝠᝣᝤᝥᝦᝧᝨᝩᝪᝫᝬᝮᝯᝰ
ᝡᝣᝲᝤᝲᝥᝲᝦᝲᝧᝲᝨᝲᝩᝲᝪᝲᝫᝲᝬᝲᝮᝲᝯᝲᝰᝲ
 ᝢ ᝣᝳᝤᝳᝥᝳᝦᝳᝧᝳᝨᝳᝩᝳᝪᝳᝫᝳᝬᝳᝮᝳᝯᝳᝰᝳ

I've never drawn any font and don't know tagbanwa at all, so you may find this
one not very elegant, etc., but at least it exists (I couldn't find any free
font for Tagbanwa...).  If you'd like to fix issues, feel free, that's what
Creative Common License is for.  That said, maybe you should send me back the
fixed font (with your name added of course) so people can find the fixed font at
the same place.

Samuel Thibault <samuel.thibault@ens-lyon.org>
